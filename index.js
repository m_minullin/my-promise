const statuses = {
  pending: "PENDING",
  fulfilled: "FULFILLED",
  rejected: "REJECTED",
};

class MyPromise {
  status;
  promiseChain = [];
  value;

  constructor(fn) {
    if (typeof fn !== "function") {
      throw new TypeError("not a function");
    }
    this.status = statuses.pending;
    try {
      fn(this.resolve.bind(this), this.reject.bind(this));
    } catch (e) {
      this.reject.bind(this)(e);
    }
  }

  resolve(data) {
    if (this.status === statuses.pending) {
      this.status = statuses.fulfilled;
      this.value = data;
      this.handle();
    }
  }

  reject(err) {
    if (this.status === statuses.pending) {
      this.status = statuses.rejected;
      this.value = err;
      this.handle();
    }
  }

  handle() {
    if (this.status === statuses.rejected && this.promiseChain.length === 0) {
      console.log("Unhandled promise rejection", this.value);
    }

    this.promiseChain.forEach((fn) => {
      setTimeout(() => {
        const callback =
          this.status === statuses.fulfilled ? fn.onResolved : fn.onRejected;
        if (callback === null) {
          if (this.status === statuses.fulfilled) {
            this.resolve.bind(fn.promise)(this.value);
          } else {
            this.reject.bind(fn.promise)(this.value);
          }
          return;
        }
        let result;
        try {
          result = callback(this.value);
        } catch (e) {
          this.reject.bind(fn.promise)(e);
        }
        this.resolve.bind(fn.promise)(result);
      }, 0);
    });
  }

  then(onResolved, onRejected) {
    const promise = new this.constructor(() => {});
    this.promiseChain.push({
      onResolved: typeof onResolved === "function" ? onResolved : null,
      onRejected: typeof onRejected === "function" ? onRejected : null,
      promise,
    });
    return promise;
  }

  catch(onRejected) {
    return this.then(null, onRejected);
  }
}
